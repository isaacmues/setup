# Personal setup

---

This is an assorted collection of scripts and guides that I've found useful.
Enjoy?

## NVIDIA

- [CUDA in a toolbox container](./guides/cuda-toolbox.md)
