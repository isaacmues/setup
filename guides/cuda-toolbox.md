# CUDA in a toolbox container

---

This is a guide to setup the CUDA inside a toolbox container on Silverble/Fedora
and is mostly taken from article by Radu Zaharia[^1], which was probably
inspired by another article by David Gray[^2].

## Host Preparation

The following commands are for Silverblue, for Fedora simply replace `rpm-ostree`
with `dnf`.
Assuming that podman is already installed we need to install the NVIDIA drivers.
For this we can follow the RPM Fusion guide.

1. Configure RPM Fusion repository[^3].

```
sudo rpm-ostree install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
                        https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
```

2. Install NVIDIA packages for CUDA[^4].

```
sudo rpm-ostree install akmod-nvidia xorg-x11-drv-nvidia-cuda
```

3. Install NVIDIA container runtime. This worked for RHEL 8.3 in 2022, however currently there is RHEL 9.0.

```
# Configure the repository
curl -s -L https://nvidia.github.io/nvidia-container-runtime/rhel8.3/nvidia-container-runtime.repo | sudo tee /etc/yum.repos.d/nvidia-container-runtime.repo

# Install the package
rpm-ostree install nvidia-container-runtime
```

4. Edit the file `/etc/nvidia-container/config.toml` to say

```
set no-cgroups = true
```

The Red Hat guide mentions the SELinux Policy, however I've not tried that method.

## Toolbox Configuration

Follow steps 1 and 2 from the previous section.

## References

[^1]: [Using CUDA in a toolbox](https://raduzaharia.medium.com/using-cuda-in-a-toolbox-container-22d71597e40)
[^2]: [How to enable NVIDIA GPUs in containers on bare metal in RHEL 8](https://www.redhat.com/en/blog/how-use-gpus-containers-bare-metal-rhel-8)
[^3]: [RPM Fusion Configuration](https://rpmfusion.org/Configuration)
[^4]: [RPM Fusion: How to NVIDIA](https://rpmfusion.org/Howto/NVIDIA?highlight=%28%5CbCategoryHowto%5Cb%29)
